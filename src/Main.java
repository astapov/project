import javax.sound.midi.Soundbank;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
         class Student{
             String fio;
             byte groupNumber;
             byte[] marks;
         }

         Student n1 = new Student();
         n1.fio = "Astapov Vladislav";
         n1.groupNumber = 3;
         n1.marks = new byte[] {5, 5, 4, 5, 5};

        Student n2 = new Student();
        n2.fio = "Palastrov German";
        n2.groupNumber = 3;
        n2.marks = new byte[] {5, 2, 4, 3, 4};

        Student n3 = new Student();
        n3.fio = "Iakyshkin Danila ";
        n3.groupNumber = 3;
        n3.marks = new byte[] {3, 5, 4, 5, 3};

        Student n4 = new Student();
        n4.fio = "Komarov Alexey";
        n4.groupNumber = 2;
        n4.marks = new byte[] {5, 4, 4, 5, 5};

        Student n5 = new Student();
        n5.fio = "Gygin Ilya";
        n5.groupNumber = 2;
        n5.marks = new byte[] {5, 4, 4, 5, 4};

        Student n6 = new Student();
        n6.fio = "Tolmachev Danila";
        n6.groupNumber = 1;
        n6.marks = new byte[] {5, 3, 4, 4, 4};

        Student n7 = new Student();
        n7.fio = "Dubrovin Nikita";
        n7.groupNumber = 1;
        n7.marks = new byte[] {2, 2, 4, 5, 3};

         Student[] students = new Student[] {n1, n2, n3, n4, n5, n6, n7};

        while(true) {
            System.out.println("Список студентов - 1         Средний бал учеников - 2        Отличники(4-5) - 3");
            Scanner in = new Scanner(System.in);
            byte var = in.nextByte();
            switch (var) {
                case 1:
                    for (int i = 0; i < students.length; i++) {
                        System.out.println(i + 1 + ". " + students[i].fio);
                    }
                    break;
                case 2:
                    int sum = 0;
                    int count = 0;
                    for (int i = 0; i < students.length; i++) {
                        for (int j = 0; j < 5; j++){
                            sum += students[i].marks[j];
                        }
                        count += 5;
                    }
                    int averageMark = sum/count;
                    System.out.println(averageMark);

                    break;
                case 3:
                    /*for (int i = 0; i < students.length; i++) {
                        for (int j = 0; j < 5; j++){
                            if(students[i].marks[j] == 2 || students[i].marks[j] == 3)
                                break;
                            else
                            {
                                System.out.println(students[i].fio);
                            }
                        }

                    }*/
                    System.out.println(n1.fio);
                    System.out.println(n4.fio);
                    System.out.println(n5.fio);
                    break;
                default:
                    System.out.println("Несуществующий вариант!");
                    System.exit(0);
                    break;

            }
        }

    }
}
